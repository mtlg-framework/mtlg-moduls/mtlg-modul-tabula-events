/**
 * @Author: thiemo
 * @Date:   2017-11-16T15:41:34+01:00
 * @Last modified by:   thiemo
 * @Last modified time: 2017-11-17T11:14:05+01:00
 */

var http = require('http');
var server = http.createServer(function(request, response) {});
var count = 0;
var clients = {};

var timer;
var width = 400;
var height = 400;
var state = 0;
var simTouches = 1;

var start = 0;

var touches = [];
var _freetouches = [];
var id = 0;


server.listen(8888, function() {
  console.log((new Date()) + ' Server is listening on port 8888');
});

var WebSocketServer = require('websocket').server;
wsServer = new WebSocketServer({
  httpServer: server
});

wsServer.on('request', function(r) {
  // Code here to run on connection
  var connection = r.accept('tabula-protocol', r.origin);
  // Specific id for this client & increment count
  var id = count++;
  // Store the connection method so we can loop through & contact all clients
  clients[id] = connection;
  console.log((new Date()) + ' Connection accepted [' + id + ']');

  if (!start) {
    timer = setInterval(function() {
      console.log("emit touch");
      touchEvent(connection);
    }, 3000);
    start = 1;
  }

  // Create event listener
  connection.on('message', function(message) {

    // The string message that was sent to us
    var msgString = message.utf8Data;

    // Loop through all clients
    for (var i in clients) {
      // Send a message to the client with the message
      clients[i].sendUTF(msgString);
    }

  });

  connection.on('close', function(reasonCode, description) {
    delete clients[id];
    console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
  });

  function touchEvent() {
    let _fString = generateUpTouchString();
    let sendString = _fString + generateTouchString();

    for (var i in clients) {
      // Send a message to the client with the message
      clients[i].sendUTF(sendString);
    }
  }

});

function concatArrToString(_arr) {
  let s = "";
  for (var i = 0; i < _arr.length; i++) {
    s += _arr[i];
  }
  return s;
}

function generateUpTouchString() {
  // ramdom touches
  let rId;
  let _touchstring = "";
  let _state = 2;
  let _p;

  if (touches.length > 1) {
    rId = getRandomInt(1, touches.length) - 1; // Id for touch up
    _p = touches[rId];

    console.log(touches.length);
    console.log(rId);
    console.log(_p);

    if (_freetouches.indexOf(rId) === -1) {
      _freetouches.push(rId);
      touches.splice(rId, 1);
    }

    // emit up Event
    _touchstring += "(" + _p.x + ", " + _p.y + ")," + rId + "," + _state + "\n";
    //_touchstring += concatArrToString(touches);


  }
  return _touchstring;
}


function generateTouchString() {
  let _touchstring = "";
  let _p;
  let _id;
  let _state = state;

  if (_freetouches.length) {
    _id = _freetouches.pop();
  } else {
    _id = touches.length;
  }

  //let l = decimalAdjust('round', Math.random() * simTouches + 1, 0);
  let l = simTouches;

  for (var i = 0; i < l; i++) {
    _p = generateTouch();
    touches.push("(" + _p.x + ", " + _p.y + ")," + _id + "," + _state + "\n");
    _touchstring += "(" + _p.x + ", " + _p.y + ")," + _id + "," + _state + "\n";
  }
  return _touchstring;
}

function generateTouch() {
  let _p = {
    x: decimalAdjust('round', Math.random() * width, -1),
    y: decimalAdjust('round', Math.random() * height, -1)
  };
  return _p;
}

function decimalAdjust(type, value, exp) {
  // If the exp is undefined or zero...
  if (typeof exp === 'undefined' || +exp === 0) {
    return Math[type](value);
  }
  value = +value;
  exp = +exp;
  // If the value is not a number or the exp is not an integer...
  if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
    return NaN;
  }
  // Shift
  value = value.toString().split('e');
  value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
  // Shift back
  value = value.toString().split('e');
  return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
}


/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 * https://stackoverflow.com/questions/1527803/generating-random-whole-numbers-in-javascript-in-a-specific-range
 */
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
