/**
 * @Author: thiemo
 * @Date:   2017-11-15T16:11:54+01:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-02-20T09:34:39+01:00
 */

window.MTLG = (function(m) {

  /**
   *
   * @namespace tabulaevents
   * @memberof MTLG
   *
   **/
  var tabulaEvents = (function() {

    /**
     * @function init - init function is called from mtlg to register the module
     * @memberof! MTLG.tabulaevents
     *
     */

     var socket;
     var stage;

    function init() {
      socket = new WebSocket('ws://127.0.0.1:8888', 'tabula-protocol');
      if (MTLG.menu.getMenuStage()) {
        stage = MTLG.menu.getMenuStage();
      }
      else {
        stage = MTLG.getStage();
      }

      var maxTouchPoints = 1000;
      var firstMessage = true;

      var _tid = 0 // counter ids for tangibles
      var ids = [];
      var tids = {};
      for (var i = 99; i > 0; i--) {
        ids.push(i);
      }

      socket.onopen = function() {
        console.log("Tabula Socket Verbindung wurde erfolgreich aufgebaut");
        socket.send("Hello WebSocket-Server! Please do what we want!");
      };

      socket.onmessage = function(messageEvent) {
        if (messageEvent.data) {
          // console.log(messageEvent.data);
          //handleTouches(msgToTouches(messageEvent.data));
          if(!firstMessage) handleArray(JSON.parse(messageEvent.data));
          else{
            firstMessage = false;
          }
          // controller.handleTouches(JSON.parse(messageEvent.data)); // json
        }
      };

      socket.onerror = function(errorEvent) {
        console.log("Error! Die Verbindung wurde unerwartet geschlossen");
        console.log(errorEvent);
      };

      socket.onclose = function(closeEvent) {
        console.log('Die Verbindung wurde geschlossen --- Code: ' + closeEvent.code + ' --- Grund: ' + closeEvent.reason);
      };


      function handleArray(tArr) {
        for (var i = 0; i < tArr.length; i++) {
          if (tArr[i].type) {
            handleTangibles(tArr[i]);
          } else {
            handleTouches(tArr[i]);
          }
        }
      }

      function handleTouches(t) {
        let e = new Event(t.touchState);
        e._state = t
        switch (t.touchState) {
          case 1:
            stage._handlePointerDown(t.identifier, e, t.positionX, t.positionY);
            break;
          case 2:
            stage._handlePointerMove(t.identifier, e, t.positionX, t.positionY);
            break;
          case 3:
            stage._handlePointerUp(t.identifier, e, true);
            break;
          default:
        }
      }

      function handleTangibles(t) {
        let id;
        let e = new Event(t.touchState);
        e.tangible = t;
        switch (t.touchState) {
          case 1:
            id = stackPop(t);
            console.log("TangibleEvent Down: " + t.touchState + " " + t.positionX + " " + t.positionY + " ID: " + id);
            stage._handlePointerDown(id, e, t.positionX, t.positionY);
            break;
          case 2:
            id = getID(t);
            //console.log("TangibleEvent: " + t.touchState + " " + t.positionX + " " + t.positionY + " ID: " + id);
            stage._handlePointerMove(id, e, t.positionX, t.positionY);
            break;
          case 3:
            id = getID(t);
            console.log("TangibleEvent Up: " + t.touchState + " " + t.positionX + " " + t.positionY + " ID: " + id);
            stage._handlePointerUp(id, e, true);
            stackPush(id, t);
            break;
          default:
        }
      }

      function getID(t){
        return tids[t.tangibleAlias];
      }

      function stackPop(t){
        if (tids[t.tangibleAlias] === undefined) {
          tids[t.tangibleAlias] = _tid;
         _tid += 1;
        }
        return tids[t.tangibleAlias];
      }

      function stackPush(id, t){
        // delete tids[t.tangibleAlias];
        // ids.push(id);
      }
    }

    function sendEvent(evt){
      socket.send(JSON.stringify(evt));
    }

    function info() {
      return {
        name: 'tabulaEvents',
        note: "MTLG-Modul to get touch and tangible events on websockets for the tabula project."
      };
    }

    //Register the tangible module with the MTLG gameframe
    m.addModule(init, info);

    return {
      init,
      info,
      sendEvent
    }
  })();

  m.tabulaEvents = tabulaEvents;
  return m;
})(window.MTLG || {});
